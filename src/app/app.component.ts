import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthPage } from '../pages/auth/auth';
import { CustomerPage } from '../pages/customer/customer';
import { ProductPage } from '../pages/product/product';
import { IdentityService } from '../providers/identity.service';
import { InvoicesPage } from '../pages/invoices/invoices';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  public rootPage: any;
  public customerPage : any;
  public productPage : any;
  public invoicesPage: any;


  @ViewChild('nav') nav: NavController;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private menuCtrl: MenuController,
    private identityService: IdentityService,
   
  ) {
    this.rootPage = AuthPage;
    this.productPage = ProductPage;
    this.customerPage = CustomerPage;
    this.invoicesPage = InvoicesPage;

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  public onLoad(page: any) {
    this.nav.setRoot(page);
    this.menuCtrl.close();
  }

  public onLogout() {
    this.identityService.removeToken();
    this.identityService.removeUserId();
    this.nav.popToRoot();
    this.menuCtrl.close();
  }

  public onBackToInvoices() {
    this.nav.push(this.invoicesPage);
    this.menuCtrl.close();
  }
}

