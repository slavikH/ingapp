import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { AuthPage } from '../pages/auth/auth';
import { LoginPage } from '../pages/login/login';
import { LogoutPage } from '../pages/logout/logout';
import { SignupPage } from '../pages/signup/signup';
import { InvoicesPage } from '../pages/invoices/invoices';

import { AuthService } from '../providers/auth.service'
import { IdentityService } from '../providers/identity.service';
import { CustomerPage } from '../pages/customer/customer';
import { ProductPage } from '../pages/product/product';
import { CreateInvoicePage } from '../pages/create-invoice/create-invoice';
import { GetDataService } from '../providers/data.service';


@NgModule({
  declarations: [
    MyApp,
    AuthPage,
    LoginPage,
    LogoutPage,
    SignupPage,
    InvoicesPage,
    CustomerPage,
    ProductPage,
    CreateInvoicePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AuthPage,
    LoginPage,
    LogoutPage,
    SignupPage,
    InvoicesPage,
    CustomerPage,
    ProductPage,
    CreateInvoicePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthService,
    IdentityService,
    GetDataService
  ]
})
export class AppModule { }
