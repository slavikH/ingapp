export interface Credentials {
    email: string;
    password: string;
}

export interface LoginResponse {
    id: string;
    ttl: number;
    userId: string;
    created: string;
  }
  
