export interface SignUp {
    name: string,
    email: string,
    dob: string,
    password: any;
    photo?: string,
    realm?: string,
    username?: string,
    emailVerified?: true,
    id: string
}