export interface ProductInterface {
    name: string,
    price: number,
    currency: string,
}