import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class IdentityService {
    
    public userId:string;
    public userToken: string;
    
    constructor( private storage: Storage){
        this.userId = 'userId';
        this.userToken = 'userToken';
    }

    public setUserId(userId: string) {
        this.storage.set(this.userId, userId);
    }

    public setUserToken(userToken) {
        this.storage.set(this.userToken, userToken)
    }

    public removeToken() {
       return this.storage.remove(this.userToken);
    }

    public removeUserId() {
        return this.storage.remove(this.userId);
    }

    public getUserToken () {
        return this.storage.get(this.userToken);
    }

}