import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http'
import { tap } from "rxjs/operators";

import { SignUp } from "../model/signup.interface";
import { Credentials, LoginResponse } from '../model/login.interface';
import { IdentityService } from "./identity.service";


@Injectable()

export class AuthService {

    public apiRoot: string;

    constructor(
        private http: HttpClient,
        private identityService: IdentityService
    ) {
        this.apiRoot = 'https://ing-invoicing.herokuapp.com/api';
    }

    public signup(value) {
        return this.http.post<SignUp>(`${this.apiRoot}/ing-users`, value);
    }

    public login(credentials: Credentials) {
        return this.http.post<LoginResponse>(`${this.apiRoot}/ing-users/login`, credentials)
            .pipe(
                tap(res => {
                    this.identityService.setUserToken(res.id);
                    this.identityService.setUserId(res.userId);
                })
            )
    }
}