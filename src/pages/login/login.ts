import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../providers/auth.service';
import { tap } from 'rxjs/operators';
import { InvoicesPage } from '../invoices/invoices';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public myForm: FormGroup;
  public invoicesPage: any;

  constructor(
    private navCtrl: NavController,
    private authService: AuthService
  ) {
    this.invoicesPage = InvoicesPage;
    this.myForm = new FormGroup({
      credentials: new FormGroup({
        email: new FormControl(null, Validators.required),
        password: new FormControl(null, Validators.required)
      })
    })

  }


  public onSubmit(valid: boolean) {
    if (valid === false) {
      console.log('error');
    }

    this.authService.login(this.myForm.controls.credentials.value)
      .pipe(
        tap(
          res => {
            this.navCtrl.push(this.invoicesPage);
          }
        )
      )
      .subscribe()

  }


}
