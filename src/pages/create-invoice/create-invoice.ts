import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-create-invoice',
  templateUrl: 'create-invoice.html',
})
export class CreateInvoicePage {

  public customers: any;
  public products: any;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
  ) {
    this.customers = this.navParams.get('customers');
    this.products = this.navParams.get('products');
    console.log(this.customers);
    console.log(this.products);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateInvoicePage');
  }

}
