import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NavController } from 'ionic-angular';

import { tap } from 'rxjs/operators';


import { AuthService } from '../../providers/auth.service';
import { InvoicesPage } from '../invoices/invoices';


@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage implements OnInit {

  public myForm: FormGroup;
  public invoicesPage: any;

  constructor(
    private authService: AuthService,
    private navCtrl: NavController
  ) {
    this.invoicesPage = InvoicesPage;
  }

  public ngOnInit() {
    this.myForm = new FormGroup({
      name: new FormControl(null, [
        Validators.required,
        Validators.pattern('[a-zA-z]+')
      ]),
      dob: new FormControl(null, Validators.required),
      email: new FormControl(null, [
        Validators.required,
        Validators.pattern(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/)
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(6)
      ])
    })
  }

  public onSubmit(values: any, valid: boolean) {
    console.log(values, valid);

    if (valid === false) {
      console.log('error');
    }

    this.authService.signup(values)
      .pipe(
        tap(
          res => {
            console.log(res)
            this.navCtrl.push(this.invoicesPage)
          }
        )
      )
      .subscribe()



  }





}
