import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CreateInvoicePage } from '../create-invoice/create-invoice';
import { IdentityService } from '../../providers/identity.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators';


import { CustomerResponse } from '../../model/customer.interface';
import { ProductInterface } from '../../model/product.interface'


@Component({
  selector: 'page-invoices',
  templateUrl: 'invoices.html',
})
export class InvoicesPage {

  public createInvoisePage: any;
  public apiRoot: string;

  constructor(
    private navCtrl: NavController,
    private identityService: IdentityService,
    private http: HttpClient
  ) {
    this.createInvoisePage = CreateInvoicePage;
    this.apiRoot = 'https://ing-invoicing.herokuapp.com/api';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InvoicesPage');
  }

  public onCreateInvoice() {
    this.getCustomersAndProducts();
  }

  public getCustomersAndProducts() {

    return this.identityService.getUserToken().then(token => {
      const headers = new HttpHeaders({
        'Authorization': token
      })

      return this.http.get<CustomerResponse>(`${this.apiRoot}/customers`, { headers })
        .pipe(
          switchMap(customers => {
            return this.http.get<ProductInterface>(`${this.apiRoot}/products`, { headers })
              .pipe(
                tap(
                  (products) => {
                    this.navCtrl.push(this.createInvoisePage, {
                      customers: customers,
                      products: products
                    });
                  }
                )
              )
          }),
        ).subscribe()
    })
  }
}
