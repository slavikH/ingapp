import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { SignupPage } from '../signup/signup';


@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html',
})
export class AuthPage {

  public loginPage: any; 
  public signUp :any;

  constructor(private navCtrl: NavController) {
  this.signUp = SignupPage;
  this.loginPage  = LoginPage;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AuthPage');
  }

  public onLogin() {
    this.navCtrl.push(this.loginPage);
  }

  public onSignup() {
    this.navCtrl.push(this.signUp);
  }

}
